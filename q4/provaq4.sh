#!/bin/bash

# Informações do processador
echo "processador."
lscpu | grep "Model name"

# informações sobre a memória
echo "Memoria RAM."
free -h | grep "mem:"

# informações sobre a placa mãe
echo "Placa mãe:"
sudo dmidecode -t baseboard | grep "Manufacturer\|product name"

# informacoes sobre dispositivos de armazenamento
echo "Dispositivos de armazenamento:" 
lsblk

# informacoes sobre placas de video
echo "Placas de video:"
lspci | grep VGA

# informacoes sobre dispositivos USB 
echo "dispositivos USB conectados:"
lsusb

# informacoes sobre a versão do sistema operacional
echo "Sistema Operacional:"
lsb_release -a

# informacoes sobre a rede 
echo "interfaces de rede:"
ip a



