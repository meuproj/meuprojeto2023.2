#!/bin/bash

read -p "digite o nome de 3 arquivos como parametros de linhas de comando:" $1 $2 $3

a=$(( ls -la $1 | grep "^_" | wc -1 ))
b=$(( ls -la $2 | grep "^_" | wc -1 ))
c=$(( ls -la $3 | grep "^_" | wc -1 ))

echo "informe o total de arquivos que tem nos 3 diretorios: $(( a + b + c))"
