#!/bin/bash

# 1. Atribuindo um valor diretamente
nome="joão"
echo "1. variavel com valor atribuido diretamente: \$nome = $nome"

# 2. Lendo um valor do usuário 
echo "2. Digite seu sobrenome:"
read sobrenome
echo "variavel lida do usuario: \$sobrenome = $sobrenome"

# 3. Recebendo valores como parametros de linha de comando
par1="$1" # o primeiro argumento passado
par2="$2" # O segundo argumento passado

echo "3.valores passados como parametros de linha de comando:"
echo "\$par1 = $par1"
echo "\$par2 = $par2"

# 4. variaveis de ambiente 
variavel_ambiente=$USER" # A variavel $USER contém o nome do usario atual
echo "4. variavel de ambiente: \$USER = $variavel_ambiente"
i
#6. Variáveis de SHELL

echo "6.variaveis de shell:"
echo "\$shell (shell padrão): $SHELL"
echo "\$HOME (diretorio home do usuario: $HOME"


