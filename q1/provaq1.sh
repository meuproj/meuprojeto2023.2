#!/bin/bash

echo "não lamente o que podia ter e se perdeu por caminhos
errados e nunca mais voltou."

sleep 2

echo "há tres coisas na vida que nunca voltam atrás
a flecha lançada, a palavra pronunciada e a 
oportunidade perdida."

sleep 2

echo "a vida so pode ser compreendida olhando-se para tras, mas só
pode ser vivida olhando-se para frente."

sleep 2

echo "maior que a tristeza de não haver vencido é a vergonha
de nao ter lutado."

sleep 2

echo "o numero dos que nos invejam confirma as nossas capacidades."

sleep 2

echo "a vida é muito importante para ser levada a serio."

sleep 2

echo "nao basta conquistar a sabedoria, é preciso usá-la"

sleep 2

echo "o importante não é vencer todos os dias, mas lutar sempre."

sleep 2

echo "viver significa lutar"

sleep 2
